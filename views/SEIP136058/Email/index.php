<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP136058\Email\Email;
use App\BITM\SEIP136058\Utility\Utility;
use App\BITM\SEIP136058\Message\Message;

$object= new Email();

$availableName=$object->getAllName();

$comma_separated= '"'.implode('","',$availableName).'"';
//print_r($comma_separated);
//Utility::d($allBook);
$availableEmail=$object->getAllEmail();
$comma_separated_Email= '"'.implode('","',$availableEmail).'"';

if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}

//Utility::dd($_SESSION['itemPerPage']);

$itemPerPage=$_SESSION['itemPerPage'];
$totalItem=$object->count();

$totalPage=ceil($totalItem/$itemPerPage);
//Utility::dd($itemPerPage);
$pagination="";


if(array_key_exists('pageNumber',$_GET)){
    $pageNumber=$_GET['pageNumber'];
}else{
    $pageNumber=1;
}
for($i=1;$i<=$totalPage;$i++){
    $class=($pageNumber==$i)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNumber-1);
//$allS=$object->paginator($pageStartFrom,$itemPerPage);

if(strtoupper($_SERVER['REQUEST_METHOD']=='GET')) {
    $allS = $object->paginator($pageStartFrom, $itemPerPage);
}
if(strtoupper($_SERVER['REQUEST_METHOD']=='POST')) {
    $allS = $object->prepare($_POST)->index();
}
if((strtoupper($_SERVER['REQUEST_METHOD']=='GET'))&& isset($_GET['search'])) {
    $allS = $object->prepare($_GET)->index();
}


?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

</head>
<body>
<br/>
<div align="right">
    <a href="../../../index.php" class="btn btn-success btn-lg" role="button">Atomic Project</a>
</div>
<div class="container">
    <h2>All Subscriber's Email List</h2>
    <a href="create.php" class="btn btn-primary" role="button">Create again</a>  <a href="trashed.php" class="btn btn-primary" role="button">View Trashed list</a> <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>   <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>   <a href="mail.php" class="btn btn-primary" role="button">Mail to friend</a>
    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }
        ?>
    </div>
    <form role="form">
        <div class="form-group">
            <label for="sel1">Select how many items you want to show (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option <?php if($itemPerPage==5):?>selected<?php endif ?>>5</option>
                <option <?php if($itemPerPage==10):?>selected<?php endif?>>10</option>
                <option <?php if($itemPerPage==15):?>selected<?php endif ?>>15</option>
                <option <?php if($itemPerPage==20):?>selected<?php endif ?>>20</option>
                <option <?php if($itemPerPage==25):?>selected<?php endif ?>>25</option>
            </select>
            <button type="submit">Go!</button>

        </div>
    </form>

    <form action="index.php" method="post">
        <label>Filter by Name</label>
        <input type="text" name="filterByName" value="" id="username">
        <label>Filter by Email</label>
        <input type="text" name="filterByEmail" value="" id="email">
        <button type="submit">Submit</button>
    </form>
    <form action="index.php" method="get">
        <label>Search</label>
        <input type="text" name="search" value="">
        <button type="submit">Search</button>
    </form>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>SL</th>
                <th>ID</th>
                <th>User Name</th>
                <th>Email</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allS as $uEmail){
                $sl++; ?>
                <td><?php echo $sl+$pageStartFrom?></td>
                <td><?php echo $uEmail-> id?></td>
                <td><?php echo $uEmail->username?></td>
                <td><?php echo $uEmail->email?></td>
                <td><a href="view.php?id=<?php echo $uEmail-> id ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $uEmail-> id ?>"  class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $uEmail->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                    <a href="trash.php?id=<?php echo $uEmail->id ?>"  class="btn btn-info" role="button">Trash</a>
                </td>

            </tr>
            <?php }?>


            </tbody>
        </table>
    </div>
    <ul class="pagination">
        <?php if($pageNumber>1):?>
            <li><a href="index.php?pageNumber=<?php echo $pageNumber-1 ?>">Previous</a></li><?php endif;?>
        <?php echo $pagination?>
        <?php if($pageNumber<$totalPage):?>
            <li><a href="index.php?pageNumber=<?php echo $pageNumber+1 ?>">Next</a></li><?php endif;?>
    </ul>
</div>
<script>
    $('#message').show().delay(2000).fadeOut();


    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>
<script>
    $( function() {
        var availableTags = [
            <?php echo $comma_separated?>
        ];
        $( "#username" ).autocomplete({
            source: availableTags
        });
    } );
    $( function() {
        var availableTags = [
            <?php echo $comma_separated_Email?>
        ];
        $( "#email" ).autocomplete({
            source: availableTags
        });
    } );
</script>


</body>
</html>
