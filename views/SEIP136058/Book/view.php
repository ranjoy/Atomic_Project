<?php
//var_dump($_GET);
include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP136058\Book\Book;
use App\BITM\SEIP136058\Book\Utility;

$book= new Book();
$book->prepare($_GET);
$show=$book->view();
//Utility::d($singleItem);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>View Book Title</h2>
  <ul class="list-group">
    <li class="list-group-item">ID: <?php echo $show->id?></li>
    <li class="list-group-item">Book Title: <?php echo $show->title?></li>

  </ul>
  <a href="index.php" class="btn btn-primary" role="button">Book List</a>
</div>

</body>
</html>