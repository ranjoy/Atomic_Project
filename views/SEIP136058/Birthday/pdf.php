<?php
include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP136058\Birthday\Birthday;

$obj= new Birthday();
$alluser=$obj->index();

$info="";
$sl=0;
foreach($alluser as $user):
    $sl++;
    $info.="<tr>";
    $info.="<td> $sl</td>";
    $info.="<td> $user->id</td>";
    $info.="<td> $user->name</td>";
    $info.="<td> $user->date</td>";
    $info.="</tr>";
endforeach;
$html= <<<BITM
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Birthday(Y-M-D)</th>
                    


              </tr>
                </thead>
                <tbody>

                  $info

                </tbody>
            </table>




BITM;


// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('All_User_Information.pdf', 'D');

