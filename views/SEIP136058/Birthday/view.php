<?php

include_once('../../../vendor/autoload.php');
use App\BITM\SEIP136058\Birthday\Birthday;
use App\BITM\SEIP136058\Utility\Utility;



$obj=new Birthday();
$obj->prepare($_GET);
$single = $obj->view();
//Utility::debug($single);
?>


<html>
<head>
    <title>View Details</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>User Details</h2>
        <label>Student name :</label> <br>
        <input type="text" name="name" value="<?php echo $single->name ?>"><br><br>
        <label>Student birthday :</label><br>
        <label><input type="text" name="date" value="<?php echo date("d-m-Y",strtotime($single->date)) ?>"> </label><br>

    <a href="index.php" role="button" class="btn btn-primary">Done</a>
    </form>
</div>

</body>
</html>


