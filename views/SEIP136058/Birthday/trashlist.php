<?php
session_start();

include_once('../../../vendor/autoload.php');
use App\BITM\SEIP136058\Birthday\Birthday;
use App\BITM\SEIP136058\Message\Message;
use App\BITM\SEIP136058\Utility\Utility;

$obj= new Birthday();
$allInfo= $obj->trashed();

?>
<html>
<head>
    <title>Trashlist</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Manage Student Details</h2>
    <?php if(array_key_exists("message",$_SESSION) && !empty($_SESSION['message'])): ?>
        <div id="message" class="alert alert-info">
            <center> <?php echo Message::message() ?></center>
        </div>
    <?php endif; ?>

    <a href="index.php" class="btn btn-primary" role="button">View Index</a>

    <form id="multiple" action="recoverSelected.php" method="post">
        <br>
        <input type="submit" value="Recover Selected" role="button" class="btn btn-primary">
        <input type="submit" id="delete_multiple" onclick="return ConfirmDelete()" value="Delete Selected" role="button" class="btn btn-danger">
        <table class="table">
            <thead>
            <tr>
                <td>Check Item</td>
                <td>ID</td>
                <td>Name</td>
                <td>Selected infos</td>
                <td>Action</td>
            </tr>
            </thead>

            <tbody>
            <?php foreach($allInfo as $info){ ?>
                <tr>
                    <td>
                        <input type="checkbox" name="mark[]" value="<?php echo $info->id ?>">
                    </td>
                    <td><?php echo $info->id?></td>
                    <td><?php echo $info->name?></td>
                    <td><?php echo date("d-m-Y",strtotime($info->date))?></td>
                    <td>
                        <a href="recover.php?id=<?php echo $info->id ?>" class="btn btn-primary" role="button">Recover</a>
                        <a href="delete.php?id=<?php echo $info->id ?>" onclick="return ConfirmDelete()"  class="btn btn-danger" role="button">Delete</a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </form>
</div>

<script>
    $('#message').show().delay(1500).fadeOut();

    function ConfirmDelete(){
        var x=confirm("Sure to delete?");
        if(x)
            return true;
        else
            return false;
    };

    $('#delete_multiple').on('click',function(){
        document.forms[0].action="deleteSelected.php";
        $('#multiple').submit();
    });
</script>
</body>
</html>


