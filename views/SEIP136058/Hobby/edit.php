<?php
include_once ('../../../vendor/autoload.php');

use App\BITM\SEIP136058\Hobby\Hobby;
use App\BITM\SEIP136058\Message\Message;
use App\BITM\SEIP136058\Utility\Utility;

$obj = new Hobby();
$obj->prepare($_GET);
$Hobby = $obj->edit();
//Utility::d($Hobby);

//echo $_GET['id'];

?>






<!DOCTYPE html>
<html lang="en">
<head>
    <title>Submitting Subscriber Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Select Your Hobbies</h2>
    <form role="form" method="post" action="update.php">
        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">

        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Playing Cricket" <?php if(in_array("Playing Cricket",$Hobby)) : ?> checked <?php endif; ?>>Playing Cricket</label>
        </div>
        <div class="checkbox disabled">
            <label><input type="checkbox" name="hobby[]" value="Playing Football" <?php if(in_array("Playing Football",$Hobby)) : ?> checked <?php endif; ?> >Playing Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Gaming" <?php if(in_array("Gaming",$Hobby)) : ?> checked <?php endif; ?> >Gaming</label>
        </div>
        <div class="checkbox disabled">
            <label><input type="checkbox" name="hobby[]" value="Watching Movies" <?php if(in_array("Watching Movies",$Hobby)) : ?> checked <?php endif; ?> >Watching Movies</label>
        </div>
        <div class="checkbox disabled">
            <label><input type="checkbox" name="hobby[]" value="Hanging Out" <?php if(in_array("Hanging Out",$Hobby)) : ?> checked <?php endif; ?> >Hanging Out</label>
        </div>
        <input type="submit" value="Submit">
    </form>
</div>

</body>
</html>
