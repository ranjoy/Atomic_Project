<?php


include_once('../../../vendor/autoload.php');
use App\BITM\SEIP136058\Utility\Utility;
use App\BITM\SEIP136058\Hobby\Hobby;

//Utility::d($_POST);

$selected= $_POST['hobby'];

$hobbies = implode(",", $selected);
//echo $hobbies;

$_POST['hobby'] = $hobbies;

$obj = new Hobby();
$obj->prepare($_POST);
$obj->store();
