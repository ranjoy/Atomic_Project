<?php
//var_dump($_GET);
include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP136058\City\City;
use App\BITM\SEIP136058\Book\Utility;

$object= new City();
$object->prepare($_GET);
$singleItem=$object->view();
//Utility::d($singleItem);
?>





<!DOCTYPE html>
<html lang="en">
<head>
    <title>View</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit User's Info</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Edit User Name:</label>
            <input type="hidden" name="id" id="title" value="<?php echo $singleItem->id?>">
            <input type="text" name="name" class="form-control"  value="<?php echo $singleItem->username?>">
        </div>
        <div class="form-group">
            <label for="sel1">Select Your city</label>
            <select class="form-control" id="sel1" name="city">
                <option <?php if($singleItem->city=="Barisal"):?>selected<?php endif ?>>Barisal</option>
                <option <?php if($singleItem->city=="Chittagong"):?>selected<?php endif ?>>Chittagong</option>
                <option <?php if($singleItem->city=="Dhaka"):?>selected<?php endif ?>>Dhaka</option>
                <option <?php if($singleItem->city=="Khulna"):?>selected<?php endif ?>>Khulna</option>
                <option <?php if($singleItem->city=="Rajshahi"):?>selected<?php endif ?>>Rajshahi</option>
                <option <?php if($singleItem->city=="Rangpur"):?>selected<?php endif ?>>Rangpur</option>
                <option <?php if($singleItem->city=="Sylhet"):?>selected<?php endif ?>>Sylhet</option>
            </select>
        </div>

        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>
