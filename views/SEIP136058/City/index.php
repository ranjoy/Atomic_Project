<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP136058\City\City;
use App\BITM\SEIP136058\Utility\Utility;
use App\BITM\SEIP136058\Message\Message;

$object= new City();

//Utility::d($allBook);

if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}

//Utility::dd($_SESSION['itemPerPage']);

$itemPerPage=$_SESSION['itemPerPage'];
$totalItem=$object->count();

$totalPage=ceil($totalItem/$itemPerPage);
//Utility::dd($itemPerPage);
$pagination="";


if(array_key_exists('pageNumber',$_GET)){
    $pageNumber=$_GET['pageNumber'];
}else{
    $pageNumber=1;
}
for($i=1;$i<=$totalPage;$i++){
    $class=($pageNumber==$i)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNumber-1);
$allCity=$object->paginator($pageStartFrom,$itemPerPage);



?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body>
<br/>
<div align="right">
    <a href="../../../index.php" class="btn btn-success btn-lg" role="button">Atomic Project</a>
</div>
<div class="container">
    <h2>All User's City List</h2>
    <a href="create.php" class="btn btn-primary" role="button">Create again</a>  <a href="trashed.php" class="btn btn-primary" role="button">View Trashed list</a>
    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }
        ?>
    </div>
    <form role="form">
        <div class="form-group">
            <label for="sel1">Select how many items you want to show (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option <?php if($itemPerPage==5):?>selected<?php endif ?>>5</option>
                <option <?php if($itemPerPage==10):?>selected<?php endif?>>10</option>
                <option <?php if($itemPerPage==15):?>selected<?php endif ?>>15</option>
                <option <?php if($itemPerPage==20):?>selected<?php endif ?>>20</option>
                <option <?php if($itemPerPage==25):?>selected<?php endif ?>>25</option>
            </select>
            <button type="submit">Go!</button>

        </div>
    </form>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>SL</th>
                <th>ID</th>
                <th>User Name</th>
                <th>City</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allCity as $uCity){
                $sl++; ?>
                <td><?php echo $sl+$pageStartFrom?></td>
                <td><?php echo $uCity-> id?></td>
                <td><?php echo $uCity->username?></td>
                <td><?php echo $uCity->city?></td>
                <td><a href="view.php?id=<?php echo $uCity-> id ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $uCity-> id ?>"  class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $uCity->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                    <a href="trash.php?id=<?php echo $uCity->id ?>"  class="btn btn-info" role="button">Trash</a>
                </td>

            </tr>
            <?php }?>


            </tbody>
        </table>
    </div>
    <ul class="pagination">
        <?php if($pageNumber>1):?>
            <li><a href="index.php?pageNumber=<?php echo $pageNumber-1 ?>">Previous</a></li><?php endif;?>
        <?php echo $pagination?>
        <?php if($pageNumber<$totalPage):?>
            <li><a href="index.php?pageNumber=<?php echo $pageNumber+1 ?>">Next</a></li><?php endif;?>
    </ul>
</div>
<script>
    $('#message').show().delay(2000).fadeOut();


    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>

</body>
</html>
