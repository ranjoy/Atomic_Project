<?php
namespace App\BITM\SEIP136058\City;
use App\BITM\SEIP136058\Message\Message;
use App\BITM\SEIP136058\Utility\Utility;

class City
{
    public $id="";
    public $name="";
    public $city="";
    public $deleted_at;
    public $conn;


    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "atomic_project") or die("Database connection failed");
    }
    public function prepare($data = "")
    {
        if (array_key_exists("name", $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists("city", $data)) {
            $this->city = $data['city'];
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }

        return  $this;

    }

    public function store(){
        $query="INSERT INTO `atomic_project`.`cities` (`username`, `city`) VALUES ('".$this->name."', '".$this->city."')";

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }

    }
    public function view(){
        $query="SELECT * FROM `atomic_project`.`cities` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }


    public function update(){

        $query = "UPDATE `atomic_project`.`cities` SET `username` = '" . $this->name . "', `city` = '" . $this->city . "' WHERE `cities`.`id` =" . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
        <div class=\"alert alert-info\">
        <strong>Success!</strong> Data has been updated  successfully.
        </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }


    public function delete(){
        $query="DELETE FROM `atomic_project`.`cities`WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Deleted!</strong> Data has been deleted successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
            <div class=\"alert alert-info\">
             <strong>Deleted!</strong> Data has not been deleted successfully.
            </div>");
            Utility::redirect("index.php");
        }

    }

    public function trash()
    {
        $this->deleted_at= time();
        $query = "UPDATE `atomic_project`.`cities` SET `deleted_at` =" . $this->deleted_at. " WHERE `cities`.`id` = " . $this->id;
        //echo $query;
        $result = mysqli_query($this->conn, $query);
        echo $result;
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
             <strong>Deleted!</strong> Data has been trashed successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
            <div class=\"alert alert-info\">
             <strong>Deleted!</strong> Data has not been trashed successfully.
            </div>");
            Utility::redirect("index.php");
        }

    }

    public function trashed()
    {
        $_allpic = array();
        $query = "SELECT * FROM `cities` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allpic[] = $row;
        }

        return $_allpic;


    }

    public function recover()
    {

        $query = "UPDATE `atomic_project`.`cities` SET `deleted_at` = NULL WHERE `cities`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
             <strong>Deleted!</strong> Data has been recovered successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
        <div class=\"alert alert-info\">
            <strong>Deleted!</strong> Data has not been recovered successfully.
        </div>");
            Utility::redirect("index.php");
        }

    }


    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomic_project`.`cities` SET `deleted_at` = NULL WHERE `cities`.`id` IN(" . $ids . ")";
            //echo $query;
            //die();
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
        <div class=\"alert alert-info\">
        <strong>Deleted!</strong> Selected Data has been recovered successfully.
        </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
        <div class=\"alert alert-info\">
        <strong>Deleted!</strong> Selected Data has not been recovered successfully.
        </div>");
                Utility::redirect("index.php");
            }

        }

    }

    public function deleteMultiple($IDs){
        //Utility::dd($IDs);
        if(count($IDs)>0){
            $ids = implode(",",$IDs);
            $query = "DELETE FROM `cities` WHERE `id` IN (".$ids.")";
            $result = mysqli_query($this->conn,$query);
            if($result){
                Message::message("Data has been deleted successfully");
                Utility::redirect("index.php");
            }

            else{

                Message::message("Error deleting data");
                Utility::redirect("index.php");
            }
        }
    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomic_project`.`cities` WHERE `deleted_at` IS NULL ";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }



    public function paginator($pageStartFrom=0,$Limit=5){
        $_allCity = array();
        $query="SELECT * FROM `cities` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allCity[] = $row;
        }

        return $_allCity;

    }

}