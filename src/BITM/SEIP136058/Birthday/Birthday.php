<?php

namespace App\BITM\SEIP136058\Birthday;

use App\BITM\SEIP136058\Message\Message;
use App\BITM\SEIP136058\Utility\Utility;

class Birthday{
    public $id='';
    public $date='';
    public $name='';
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomic_project");
    }


    public function prepare($data=''){
        if(array_key_exists("id",$data))
            $this->id=$data['id'];

        if(array_key_exists("date",$data))
            $this->date=$data['date'];

        if(array_key_exists("name",$data))
            $this->name=$data['name'];
    }




    public function store(){
        //Utility::dd($this->date);
        $query = "INSERT INTO `atomic_project`.`birthday` (`name`, `date`) VALUES ('".$this->name."', '".$this->date."')";
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been stored successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR!";
    }

    public function index()
    {
        $_alluser = array();
        $query = "SELECT * FROM `birthday` WHERE `deleted_at` IS NULL ";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_alluser[] = $row;
        }

        return $_alluser;


    }



    public function view(){
        $single=array();
        $query = "SELECT * FROM  `birthday` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        //
        if($row=mysqli_fetch_object($result))
            $single=$row;

        return $single;
    }


    public function update(){
       // echo $this->date;
        //die();
        $query = "UPDATE `atomic_project`.`birthday` SET `name` = '".$this->name."', `date` = '".$this->date."' WHERE `birthday`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been updated successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR";
    }


    public function trash(){
        $query = "UPDATE `atomic_project`.`birthday` SET `deleted_at` = ".time()." WHERE `birthday`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been sent to trash successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR";
    }

    public function recover()
    {

        $query = "UPDATE `atomic_project`.`birthday` SET `deleted_at` = NULL WHERE `birthday`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
        <div class=\"alert alert-info\">
         <strong>Deleted!</strong> Data has been recovered successfully.
        </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
        <div class=\"alert alert-info\">
        <strong>Deleted!</strong> Data has not been recovered successfully.
            </div>");
            Utility::redirect("index.php");
        }

    }


    public function delete(){
        $query = "DELETE FROM `atomic_project`.`birthday` WHERE `birthday`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been deleted successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR";
    }


    public function trashed(){
        $allCourse=array();
        $query = "SELECT * FROM `birthday` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)) {
            $allCourse[] = $row;
        }

        return $allCourse;
    }


    public function recoverSelected($IDs=array()){
        if(count($IDs>0)){
            $ids= implode(",",$IDs);
            ///Utility::dd($ids);
            $query = "UPDATE `atomic_project`.`birthday` SET `deleted_at` = NULL WHERE `birthday`.`id` IN (".$ids.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("Data has been recovered successfully");
                Utility::redirect("index.php");
            }
            else
                echo "ERROR";
        }

    }

    public function deleteSelected($IDs=array()){
        if(count($IDs>0)){
            $ids= implode(",",$IDs);

            $query = "DELETE FROM `atomic_project`.`birthday` WHERE `birthday`.`id` IN (".$ids.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("Data has been deleted successfully");
                Utility::redirect("index.php");
            }
            else
                echo "ERROR";
        }
    }


    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomic_project`.`birthday` WHERE `deleted_at` IS NULL ";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }



    public function paginator($pageStartFrom=0,$Limit=5){
        $_allDay = array();
        $query="SELECT * FROM `birthday` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allDay[] = $row;
        }

        return $_allDay;

    }
}