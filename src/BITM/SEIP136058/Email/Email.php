<?php

namespace App\BITM\SEIP136058\Email;
use App\BITM\SEIP136058\Message\Message;
use App\BITM\SEIP136058\Utility\Utility;
class Email
{
    public $id = "";
    public $username = "";
    public $email = "";
    public $filterByName="";
    public $filterByEmail="";
    public $search="";
    public $deleted_at;
    public $conn;

    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "atomic_project")or die("Database connection failed");
    }

    public function prepare($data)
    {
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists("username", $data)) {
            $this->username = $data['username'];
        }
        if (array_key_exists("email", $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists("filterByName", $data)) {
            $this->filterByName = $data['filterByName'];
        }
        if (array_key_exists("filterByEmail", $data)) {
            $this->filterByEmail = $data['filterByEmail'];
        }
        if (array_key_exists("search", $data)) {
            $this->search = $data['search'];
        }

        return $this;
        //echo $this->name;
        // echo $this->email;
    }

    public function store()
    {
        $query = "INSERT INTO  `subscribers` (`username`,`email`) VALUES ('" . $this->name . "','" . $this->email . "')";
        //echo $query;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function index()
    {
        $whereClause= " 1=1 ";
        if(!empty($this->filterByName)){
            $whereClause.=" AND  username LIKE '%".$this->filterByName."%'";
        }
        if(!empty($this->filterByEmail)){
            $whereClause.=" AND  email LIKE '%".$this->filterByEmail."%'";
        }
        if(!empty($this->search)){
            $whereClause.=" AND  email LIKE '%".$this->search."%' OR username LIKE '%".$this->search."%'";
        }

        $_alluser = array();
        $query = "SELECT * FROM `subscribers` WHERE `deleted_at` IS NULL ";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_alluser[] = $row;
        }

        return $_alluser;


    }


    public function count()
    {
        $query = "SELECT COUNT(*) AS totalItem FROM `atomic_project`.`subscribers` WHERE `deleted_at` IS NULL ";
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }


    public function paginator($pageStartFrom = 0, $Limit = 5)
    {
        $_allS = array();
        $query = "SELECT * FROM `subscribers` WHERE `deleted_at` IS NULL LIMIT " . $pageStartFrom . "," . $Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allS[] = $row;
        }

        return $_allS;

    }


    public function view()
    {
        $query = "SELECT * FROM `atomic_project`.`subscribers` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }

    public function update()
    {
        if (!empty($this->name)) {
            $query = "UPDATE `atomic_project`.`subscribers` SET `username` = '" . $this->name . "', `email` = '" . $this->email . "' WHERE `subscribers`.`id` =" . $this->id;
        } else {
            $query = "UPDATE `atomic_project`.`sunscribers` SET `username` = '" . $this->name . "' WHERE `subscribers`.`id` =" . $this->id;

        }

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Success!</strong> Data has been updated  successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `atomic_project`.`subscribers` WHERE `subscribers`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomic_project`.`subscribers` SET `deleted_at` =" . $this->deleted_at . " WHERE `subscribers`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\"><strong>Deleted!</strong>
                                Data has been trashed successfully.
                                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("<div class=\"alert alert-info\">
                    <strong>Deleted!</strong>Data has not been trashed successfully.
                        </div>");
            Utility::redirect("index.php");
        }

    }

    public function trashed()
    {
        $_allemail = array();
        $query = "SELECT * FROM `subscribers` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        //echo $result;
        while ($row = mysqli_fetch_object($result)) {
            $_allemail[] = $row;
        }

        return $_allemail;


    }
    public function recover()
    {

        $query = "UPDATE `atomic_project`.`subscribers` SET `deleted_at` = NULL WHERE `subscribers`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been recovered successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been recovered successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function deleteMultiple($IDs){
        //Utility::dd($IDs);
        if(count($IDs)>0){
            $ids = implode(",",$IDs);
            $query = "DELETE FROM `subscribers` WHERE `id` IN (".$ids.")";
            $result = mysqli_query($this->conn,$query);
            if($result){
                Message::message("Data has been deleted successfully");
                Utility::redirect("index.php");
            }

            else{

                Message::message("Error deleting data");
                Utility::redirect("index.php");
            }
        }
    }
    public function recovermultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomic_project`.`subscribers` SET `deleted_at` = NULL WHERE `subscribers`.`id` IN(" . $ids . ")";
            //echo $query;
            //die();
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been recovered successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been recovered successfully.
</div>");
                Utility::redirect("index.php");
            }

        }

    }
    public function getAllName()
    {
        $_allEmail = array();
        $query = "SELECT `username` FROM `subscribers` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allEmail[] = $row['username'];
        }

        return $_allEmail;


    }

    public function getAllEmail()
    {
        $_allEmail = array();
        $query = "SELECT `email` FROM `subscribers` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allEmail[] = $row['email'];
        }

        return $_allEmail;


    }


}